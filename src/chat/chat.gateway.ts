import {
  ConnectedSocket,
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { OnModuleInit } from '@nestjs/common';
import { Server, Socket } from 'socket.io';

import { ChatService } from './chat.service';

@WebSocketGateway()
export class ChatGateway implements OnModuleInit {
  @WebSocketServer()
  public server: Server;

  constructor(private readonly chatService: ChatService) {}

  onModuleInit() {
    this.server.on('connection', (socket: Socket) => {
      const { name, token } = socket.handshake.auth;
      console.log({ name, token });
      if (!name) {
        socket.disconnect;
        return;
      }

      //agrega cliente al objeto
      this.chatService.onClientConnected({ id: socket.id, name: name });

      //sms de bienvenida
      socket.emit('welcome-message', 'bienvenido al server');

      //listado de clientes conectados
      this.server.emit('on-clientes-changed', this.chatService.getClients());

      //cuando usuarios se desconecta tambien se dispara la funcion de la lista
      socket.on('disconnect', () => {
        this.chatService.onClientDisconnected(socket.id);
        this.server.emit('on-clientes-changed', this.chatService.getClients());
        // console.log('Cliente desconectado:', socket.id);
      });
    });
  }

  //los mensajes no son disparados automaticamente por el servidor
  @SubscribeMessage('send-message')
  handleMessage(
    @MessageBody() message: string,
    @ConnectedSocket() client: Socket,
  ) {
    const { name, token } = client.handshake.auth;
    console.log(name, message);
    if (!message) {
      return;
    }

    this.server.emit('on-message', {
      userId: client.id,
      message: message,
      name: name,
    });
  }
}
